<?php

return [
    'timezone' => 'Europe/Moscow',
    'sources'  => [
        '/root/sh/docker/postgrespro96/data/pg_log/postgresql-Fri.log'
        , '/root/sh/docker/postgrespro96/data/pg_log/postgresql-Mon.log'
        , '/root/sh/docker/postgrespro96/data/pg_log/postgresql-Sat.log'
        , '/root/sh/docker/postgrespro96/data/pg_log/postgresql-Sun.log'
        , '/root/sh/docker/postgrespro96/data/pg_log/postgresql-Thu.log'
        , '/root/sh/docker/postgrespro96/data/pg_log/postgresql-Tue.log'
        , '/root/sh/docker/postgrespro96/data/pg_log/postgresql-Wed.log'
        , '/var/www/project1/logs/error.log'
        , '/var/www/project1/api/runtime/logs/app.log'
        , '/var/www/project1/backend/runtime/logs/app.log'
        , '/var/www/project1/frontend/runtime/logs/app.log'
    ],
    'server'   => 'My Web Server 127.0.0.1',
	'botUrl'   => '4j4j4j4j4j43j4j4j4j4j4j4j4j4j4j4j4j4j4',
    'botId'    => '4j4j4j4j4j43j4j4j4j4j4j4j4j4j4j4j4j4j4',
    'chatId'   => 11111111111
];

<?php
echo 'Log monitor'.PHP_EOL;
$conf = require('config.php');

$dbPath = dirname(__FILE__).DIRECTORY_SEPARATOR."data.db";

if (!file_exists($dbPath)) {
    createDb($dbPath);
}

date_default_timezone_set($conf['timezone']);

$logFiles = [];

foreach ($conf['sources'] AS $path) {
    if (!file_exists($path)) {
        continue;
    }
    $logFiles[$path] = [filemtime($path), filesize($path)];
}

try {
    $file_db = new PDO('sqlite:'.$dbPath);
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result  = $file_db->query('SELECT * FROM logs');

    $data = [];

    foreach ($result as $m) {
        $data[$m['path']] = [$m['modifed_at'], $m['size']];
    }

    $newData = []; // ������ ��� ������� (����� ����), ���� � $sources �������� ����� ����

    foreach ($logFiles AS $file => $mtimeAndSize) {
        if (!isset($data[$file])) {
            $newData[$file] = $mtimeAndSize;
        }
    }

    // ������� ����� �����
    if (count($newData) > 0) {
		echo '..';
        $insert = "INSERT INTO logs (path, modifed_at, size) VALUES (:path, :modifed_at, :size)";
        $stmt   = $file_db->prepare($insert);
        $stmt->bindParam(':path', $path);
        $stmt->bindParam(':modifed_at', $modifed_at);
        $stmt->bindParam(':size', $size);

        foreach ($newData AS $p => $mAs) {
            $path       = $p;
            $modifed_at = $mAs[0];
            $size       = $mAs[1];
            $stmt->execute();

            $data[$p] = $mAs; // ������������� ����� �� ����
        }
    }

    $isertUpd = [];
    $notifs   = [$conf['server']];

    foreach ($logFiles AS $file => $mtimeAndSize) {

        // ���� ���������� ���� ��������������
        if ($mtimeAndSize[0] != $data[$file][0]) {
			
			// ��������� ����� ������ � ��� ������, ���� ������ ����� ��� ��������
			if($mtimeAndSize[1] > $data[$file][1]){
				$notifs[]        = "*$file* was changed at " . date("d.m.Y H:i:s", $mtimeAndSize[0]) . " _(Europe/Moscow)_";
			}
            
            $isertUpd[$file] = $mtimeAndSize;
        }
    }

    if (count($isertUpd) > 0) {
        $insert = "INSERT OR REPLACE INTO logs (path, modifed_at, size) VALUES (:path, :modifed_at, :size)";
        $stmt   = $file_db->prepare($insert);
        $stmt->bindParam(':path', $pathI);
        $stmt->bindParam(':modifed_at', $modifed_atI);
        $stmt->bindParam(':size', $sizeI);

        foreach ($isertUpd AS $p => $mAs) {
            $pathI       = $p;
            $modifed_atI = $mAs[0];
            $sizeI       = $mAs[1];
            $stmt->execute();
        }
    }

    if (count($notifs) > 1) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $conf['botUrl'] . $conf['botId'] . "/sendMessage");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'chat_id'    => $conf['chatId'],
            'text'       => implode("\n", $notifs),
            'parse_mode' => 'Markdown'
        ]));
        
        ob_start();
        $res = curl_exec($ch);
        ob_get_clean();
        
        curl_close($ch);
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}

function createDb($dbPath) {
    try {
        $file_db = new PDO('sqlite:'.$dbPath);
        $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $file_db->exec('CREATE TABLE "logs" ('
                . '`path` TEXT NOT NULL,'
                . '`modifed_at` INTEGER DEFAULT NULL,'
                . '`size` INTEGER NOT NULL DEFAULT 0, PRIMARY KEY(`path`)'
                . ')');
    } catch (PDOException $ex) {
        echo $ex->getMessage();
    }
}

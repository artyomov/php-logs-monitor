# USE #

Create config via copy `cp config_example.php config.php`

Edit config.php and add cron task:

`* * * * * /bin/php /root/php-logs-monitor/log_monitor.php >/dev/null 2>&1`
